# SilverStripe Recipe

```javascript
{
    "name": "fdp/{{ PROJECT_NAME }}",
    "authors": [
        {
            "name": "{{ AUTHOR_NAME }}",
            "email": "{{ AUTHOR_EMAIL }}"
        }
    ],
    "repositories": [
        {
            "type": "vcs",
            "url": "https://fdpagency@bitbucket.org/fdpagency/silverstripe-common.git"
        },
        {
            "type": "vcs",
            "url": "https://fdpagency@bitbucket.org/fdpagency/silverstripe-recipe.git"
        }
    ],
    "config": {
        "bitbucket-oauth": {
            "bitbucket.org": {
                "consumer-key": "{{ CONSUMER_KEY }}",
                "consumer-secret": "{{ CONSUMER_SECRET }}"
            }
        }
    },
    "minimum-stability": "dev",
    "require": {
        "fdp/recipe": "dev-master"
    },
    "extra": {
        "public-files-installed": [
            "install-frameworkmissing.html",
            "install.php"
        ]
    }
}
```
